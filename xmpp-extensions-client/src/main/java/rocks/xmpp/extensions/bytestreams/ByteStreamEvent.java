/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 Christian Schudt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package rocks.xmpp.extensions.bytestreams;

import java.io.IOException;
import java.util.EventObject;

/**
 * @author Christian Schudt
 */
public abstract class ByteStreamEvent extends EventObject {

    private final String sessionId;

    /**
     * Constructs a byte stream event.
     *
     * @param source    The object on which the Event initially occurred.
     * @param sessionId The session id.
     * @throws IllegalArgumentException if source is null.
     */
    protected ByteStreamEvent(Object source, String sessionId) {
        super(source);
        this.sessionId = sessionId;
    }

    /**
     * Gets the session id.
     *
     * @return The session id.
     */
    public final String getSessionId() {
        return sessionId;
    }

    /**
     * Accepts the session.
     *
     * @return The byte stream session.
     * @throws java.io.IOException If the byte stream session could not be established.
     */
    public abstract ByteStreamSession accept() throws IOException;

    /**
     * Rejects the session.
     */
    public abstract void reject();
}
